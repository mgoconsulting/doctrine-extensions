<?php

namespace Mgo\DoctrineExtension\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Mgo Doctrine Extension.
 */
class MgoDoctrineExtension extends Extension
{
    public function getAlias()
    {
        return 'mgo_doctrine_extension';
    }

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        if ($config['sequenceable']) {
            $loader->load('sequenceable.yml');
        }
        if ($config['dependson']) {
            $loader->load('dependson.yml');
        }
        if ($config['origin']['enabled']) {
            $loader->load('origin.yml');
            $container->setParameter('mgo.doctrine_extension.origin.config', $config['origin']);
        }
        $loader->load('validator.yml');
    }
}
