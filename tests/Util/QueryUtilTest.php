<?php

namespace Test\Service;

use Doctrine\ORM\EntityManagerInterface;
use Mgo\DoctrineExtension\Util\QueryUtil;
use Test\TestCase;
use Test\Entity\OriginTestEntity;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Helper\ProgressBar;

class QueryUtilTest extends TestCase
{
    /** @var int */
    private const NB_ELEMENTS = 50;

    /** @var \Doctrine\ORM\EntityManager */
    protected static $em;
    /** @var array */
    protected static $sizes = [];

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        // create lots of data for this test
        self::$em = self::$doctrine->getManagerForClass(OriginTestEntity::class); // @phpstan-ignore-line

        for ($i = 0; $i < self::NB_ELEMENTS; $i++) {
            $entity = new OriginTestEntity();
            self::$em->persist($entity);
        }
        self::$em->flush();
    }

    public static function tearDownAfterClass(): void
    {
        // truncate all
        self::$em->createQuery('DELETE ' . OriginTestEntity::class)->execute();
    }

    public function setUp(): void
    {
        // important for use of doctrine unitOfWork size
        self::$sizes = [];
        self::$em->clear();
    }

    public function testPreAndPostPageClosure()
    {
        /** @var \Doctrine\ORM\QueryBuilder $qb */
        $qb = self::$em->getRepository(OriginTestEntity::class)->createQueryBuilder('o');
        $qbTest = clone $qb;
        $qbTest->select('count(o.id)');
        $nbResult = $qbTest->getQuery()->getSingleScalarResult();

        $pageSize = 10;
        $nbPage = ceil($nbResult / $pageSize);

        $nbPre = $nbPost = 0;

        QueryUtil::applyPaginated(
            $qb->getQuery(),
            function ($item, $context) {
            },
            [
                'page_size' => $pageSize,
                'pre_page_closure' => function (array $context) use (&$nbPre) {
                    ++$nbPre;
                },
                'post_page_closure' => function (array $context) use (&$nbPost) {
                    ++$nbPost;
                },
            ]
        );
        $this->assertEquals($nbPage, $nbPre, 'Number Pre page closure pass');
        $this->assertEquals($nbPage, $nbPost, 'Number Post page closure pass');
    }

    public function applyPaginatedData()
    {
        return [
            'default options' => [
                function ($item, $options) {
                    $this->assertNull($options['progress_bar'], 'progress bar');
                },
                [],
            ],
            'page size option = 5' => [
                function ($item, $options) {
                    $this->assertPageSize($options, 5);
                },
                ['page_size' => 5],
            ],
            'page size option = 19' => [
                function ($item, $options) {
                    $this->assertPageSize($options, 19);
                },
                ['page_size' => 19],
            ],
            'page size option = 99999' => [
                function ($item, $options) {
                    $this->assertPageSize($options, 99999);
                },
                ['page_size' => 99999],
            ],
            'progress bar option' => [
                function ($item, $options) {
                    // check progress bar
                    $this->assertInstanceOf(
                        ProgressBar::class,
                        $options['progress_bar'],
                        'progress bar'
                    );
                    /** @var ProgressBar $progressBar */
                    $progressBar = $options['progress_bar'];
                    $this->assertEquals(self::NB_ELEMENTS, $progressBar->getMaxSteps(), 'progress_bar max steps');
                },
                ['instantiate_progress_bar' => new NullOutput()],
            ],
        ];
    }

    /**
     * @dataProvider applyPaginatedData
     */
    public function testApplyPaginated(callable $assertCallback, array $options = [])
    {
        $expectedPage = $expectedIndex = 1;
        /** @var \Doctrine\ORM\QueryBuilder $qb */
        $qb = self::$em->getRepository(OriginTestEntity::class)->createQueryBuilder('o');

        QueryUtil::applyPaginated(
            $qb->getQuery(),
            function ($item, $context) use (&$expectedIndex, &$expectedPage, $assertCallback, $options) {
                // defined page size
                $pageSize = $options['page_size'] ?? QueryUtil::DEFAULT_PAGE_SIZE;
                // item is an element
                $this->assertInstanceOf(OriginTestEntity::class, $item);
                $this->assertEquals($expectedIndex, $item->getId(), 'item id');
                // optionsis array
                $this->assertIsArray($context, 'options is array');
                // count
                $this->assertArrayHasKey('count', $context);
                $this->assertEquals(self::NB_ELEMENTS, $context['count'], 'count');
                // entity manager
                $this->assertArrayHasKey('entity_manager', $context);
                $this->assertInstanceOf(EntityManagerInterface::class, $context['entity_manager'], 'entity manager');
                // current page number
                $this->assertArrayHasKey('page', $context);
                $this->assertEquals($expectedPage, $context['page'], 'current page');
                // current page index number
                $this->assertArrayHasKey('page_index', $context);
                $this->assertEquals(
                    $expectedIndex - (($expectedPage - 1) * $pageSize),
                    $context['page_index'],
                    'current page index'
                );
                // current index
                $this->assertArrayHasKey('index', $context);
                $this->assertEquals($expectedIndex, $context['index'], 'current index');
                // progress bar
                $this->assertArrayHasKey('progress_bar', $context);
                // custom asserts
                $assertCallback($item, $context, $expectedIndex);
                // expected
                if (0 === ($expectedIndex % $pageSize)) {
                    $expectedPage++;
                }
                $expectedIndex++;
            },
            $options
        );
    }

    private function assertPageSize(array $options, int $pageSize)
    {
        // size increment everytime a query is made (numer of select)
        self::$sizes[] = self::$em->getUnitOfWork()->size();
        // catch last iteration
        if ($options['index'] === $options['count']) {
            $this->assertEquals(
                count(\array_unique(self::$sizes)),
                ceil(self::NB_ELEMENTS / $pageSize),
                'number of DB queries'
            );
        }
    }
}
