<?php

namespace Mgo\DoctrineExtension\Sequenceable\Service;

use Doctrine\Persistence\ObjectManager;
use Mgo\DoctrineExtension\Exception\SequenceableException;
use Mgo\DoctrineExtension\Sequenceable\SequenceableHelper;

abstract class AbstractFormatterSequenceableService implements SequenceableServiceInterface
{
    abstract protected function getFormat(): string;

    public function next(ObjectManager $em, object $object, string $field, array $configuration)
    {
        $alias = 't';
        // query builder helper
        $qbHelper = new SequenceableHelper($em);
        // get PropertyAccessor
        $pa = SequenceableHelper::getPropertyAccessor();

        $format = date($this->getFormat());

        // validate the sprintf format
        if (!\is_string(@\sprintf($format, 'just for testing the sprintf format'))) {
            $message = "Impossible to generate sequence from \"{$format}\"." . PHP_EOL;
            $message .= 'Maybe the format has too much "sprintf" (%x) arguments ?';
            throw new SequenceableException($message);
        }

        // @TODO check ici que %d est bien present dans format

        $qb = $qbHelper->getLastItemOfSequenceQueryBuilder($object, $field, $configuration, $alias);

        $qb->andWhere($qb->expr()->like("{$alias}.{$field}", ':format'));
        $qb->setParameter('format', \str_replace('%d', '%', $format));

        // override orderBy already set before to perform a natural sorting
        $qb->orderBy("LENGTH({$alias}.{$field})", 'DESC');
        $qb->addOrderBy("{$alias}.{$field}", 'DESC');
        $qb->setMaxResults(1);

        // find last element in the sequence
        $res = $qb->getQuery()->getResult();

        // get last numero
        $newValue = $configuration['start_at'] ?? 1;
        if (is_array($res) && 1 === count($res)) {
            // last value found
            $last = $pa->getValue($res[0], $field);
            // create regexp with tmp replace hacky stuff
            $tmp = \str_repeat('TMP', 20);
            $regex = \preg_quote(\str_replace('%d', $tmp, $format));
            $regex = \sprintf('#%s#', \str_replace($tmp, '(\d+)', $regex));
            // apply regexp
            $matches = [];
            if (\preg_match($regex, $last, $matches)) {
                // increment new value
                $newValue = (int) $matches[1] + 1;
            }
        }

        return @\sprintf($format, $newValue);
    }
}
