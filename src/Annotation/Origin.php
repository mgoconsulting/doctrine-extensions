<?php

namespace Mgo\DoctrineExtension\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * DependsOn annotation.
 *
 * @Annotation
 * @Target("PROPERTY")
 */
final class Origin extends Annotation
{
    /** @var string */
    public $service = 'mgo.doctrine_extension.default_origin_service'; // default service name
}
