<?php

namespace Test;

use Test\Entity\DependsOnTestEntity;

class DependsOnTest extends TestCase
{
    public function dataXor()
    {
        $error = '"b" field depends on "a": One or the other should be set, not both';

        return [
            'a=null b=null' => [null, null, $error],
            'a=false b=null' => [false, null],
            'a=null b=1' => [null, 1],
            'a=0 b=1' => [0, 1, $error],
        ];
    }

    /**
     * @dataProvider dataXor
     */
    public function testXor($a, $b, $expectedError = false)
    {
        $entity = new DependsOnTestEntity();
        $entity->setA($a);
        $entity->setB($b);
        $entity->setC(true); // always true to pass 'and' operator
        $entity->setD(true); // always true to pass 'and' operator
        $entity->setE(null); // always null to pass 'or' operator
        $entity->setF(true); // always true to pass 'or' operator
        $entity->setG(null); // always null to pass 'not_and' operator
        $entity->setH(null); // always null to pass 'not_and' operator
        /** @var \Symfony\Component\Validator\ConstraintViolationList $errors */
        $errors = self::$validator->validate($entity);
        if ($expectedError) {
            $this->assertStringContainsString($expectedError, (string) $errors);
        } else {
            $this->assertEmpty($errors, "Error: {$expectedError}");
        }
        $em = self::$doctrine->getManagerForClass(DependsOnTestEntity::class);
        $em->persist($entity);
        $em->flush();
    }

    public function dataAnd()
    {
        $error = '"d" field depends on "c": both should be set';

        return [
            'a=null b=null' => [null, null, $error],
            'a=false b=null' => [false, null, $error],
            'a=null b=1' => [null, 1, $error],
            'a=0 b=1' => [0, 1],
        ];
    }

    /**
     * @dataProvider dataAnd
     */
    public function testAnd($c, $d, $expectedError = false)
    {
        $entity = new DependsOnTestEntity();
        $entity->setA(null); // always null to pass 'orx' operator
        $entity->setB(true); // always true to pass 'orx' operator
        $entity->setC($c);
        $entity->setD($d);
        $entity->setE(null); // always null to pass 'or' operator
        $entity->setF(true); // always true to pass 'or' operator
        $entity->setG(null); // always null to pass 'not_and' operator
        $entity->setH(null); // always null to pass 'not_and' operator
        /** @var \Symfony\Component\Validator\ConstraintViolationList $errors */
        $errors = self::$validator->validate($entity);
        if ($expectedError) {
            $this->assertStringContainsString($expectedError, (string) $errors);
        } else {
            $this->assertEmpty($errors, "Error: {$expectedError}");
        }
        $em = self::$doctrine->getManagerForClass(DependsOnTestEntity::class);
        $em->persist($entity);
        $em->flush();
    }

    public function dataOr()
    {
        $error = '"f" field depends on "e": at least one of these fields should be set';

        return [
            'a=null b=null' => [null, null, $error],
            'a=false b=null' => [false, null],
            'a=null b=1' => [null, 1],
            'a=0 b=1' => [0, 1],
        ];
    }

    /**
     * @dataProvider dataOr
     */
    public function testOr($e, $f, $expectedError = false)
    {
        $entity = new DependsOnTestEntity();
        $entity->setA(null); // always null to pass 'orx' operator
        $entity->setB(true); // always true to pass 'orx' operator
        $entity->setC(true); // always true to pass 'and' operator
        $entity->setD(true); // always true to pass 'and' operator
        $entity->setE($e);
        $entity->setF($f);
        $entity->setG(null); // always null to pass 'not_and' operator
        $entity->setH(null); // always null to pass 'not_and' operator
        /** @var \Symfony\Component\Validator\ConstraintViolationList $errors */
        $errors = self::$validator->validate($entity);
        if ($expectedError) {
            $this->assertStringContainsString($expectedError, (string) $errors);
        } else {
            $this->assertEmpty($errors, "Error: {$expectedError}");
        }
        $em = self::$doctrine->getManagerForClass(DependsOnTestEntity::class);
        $em->persist($entity);
        $em->flush();
    }

    public function dataNotAnd()
    {
        $error = '"h" field depends on "g": Not both fields can be set';

        return [
            'a=null b=null' => [null, null],
            'a=false b=null' => [false, null],
            'a=null b=1' => [null, 1],
            'a=0 b=1' => [0, 1, $error],
        ];
    }

    /**
     * @dataProvider dataNotAnd
     */
    public function testNotAnd($g, $h, $expectedError = false)
    {
        $entity = new DependsOnTestEntity();
        $entity->setA(null); // always null to pass 'orx' operator
        $entity->setB(true); // always true to pass 'orx' operator
        $entity->setC(true); // always true to pass 'and' operator
        $entity->setD(true); // always true to pass 'and' operator
        $entity->setE(null); // always null to pass 'or' operator
        $entity->setF(true); // always true to pass 'or' operator
        $entity->setG($g);
        $entity->setH($h);
        /** @var \Symfony\Component\Validator\ConstraintViolationList $errors */
        $errors = self::$validator->validate($entity);
        if ($expectedError) {
            $this->assertStringContainsString($expectedError, (string) $errors);
        } else {
            $this->assertEmpty($errors, "Error: {$expectedError}");
        }
        $em = self::$doctrine->getManagerForClass(DependsOnTestEntity::class);
        $em->persist($entity);
        $em->flush();
    }
}
