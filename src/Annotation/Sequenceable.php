<?php

namespace Mgo\DoctrineExtension\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * Sequenceable annotation.
 *
 * @Annotation
 * @Target("PROPERTY")
 */
final class Sequenceable extends Annotation
{
    /** @var string */
    public $grouping = null;
    /** @var int */
    public $startAt = 1;
    /** @var int */
    public $step = 1;
    /** @var string */
    public $service = null;
}
