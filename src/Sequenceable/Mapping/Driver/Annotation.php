<?php

namespace Mgo\DoctrineExtension\Sequenceable\Mapping\Driver;

use Gedmo\Exception\InvalidMappingException;
use Gedmo\Mapping\Driver\AbstractAnnotationDriver;
use Mgo\DoctrineExtension\Annotation\Sequenceable;

class Annotation extends AbstractAnnotationDriver
{
    /**
     * {@inheritdoc}
     */
    public function readExtendedMetadata($meta, array &$config)
    {
        $class = $this->getMetaReflectionClass($meta);

        // property annotations
        foreach ($class->getProperties() as $property) {
            if ($sequenceable = $this->reader->getPropertyAnnotation($property, Sequenceable::class)) {
                $field = $property->getName();
                if (!$meta->hasField($field)) {
                    $message = "Unable to find sequenceable [{$field}] as mapped property in entity {$class}";
                    throw new InvalidMappingException($message);
                }

                if ($sequenceable->grouping) {
                    foreach ((array) $sequenceable->grouping as $grouping) {
                        if (!$meta->hasField($grouping) && !$meta->hasAssociation($grouping)) {
                            $message = sprintf(
                                "Sequenceable grouping '%s' is not a field or association in entity %s",
                                $grouping,
                                $class
                            );
                            throw new InvalidMappingException($message);
                        }
                    }
                }

                $step = (int) $sequenceable->step;
                if (!$step) {
                    $message = "Invalid sequenceable step '{$sequenceable->step}' in entity {$class}";
                    throw new InvalidMappingException($message);
                }

                $startAt = (int) $sequenceable->startAt;
                if (!\is_int($startAt)) { // could be negative or zero
                    $message = "Invalid sequenceable startAt '{$sequenceable->startAt}' in entity {$class}";
                    throw new InvalidMappingException($message);
                }

                $service = $sequenceable->service;
                if ($service && !\is_scalar($service)) { // could be negative or zero
                    $message = "Invalid sequenceable service in entity {$class}. Scalar expected";
                    throw new InvalidMappingException($message);
                }

                $config['sequenceable']['fields'][$field] = [
                    'grouping' => $sequenceable->grouping,
                    'start_at' => $startAt,
                    'step' => $step,
                    'service' => $service,
                ];
            }
        }
    }
}
