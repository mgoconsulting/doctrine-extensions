<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mgo\DoctrineExtension\Origin\Traits\OriginableEntity;

/**
 * @ORM\Table(name="Origin")
 * @ORM\Entity(repositoryClass="Doctrine\ORM\EntityRepository")
 */
class OriginTestEntity
{
    use OriginableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
