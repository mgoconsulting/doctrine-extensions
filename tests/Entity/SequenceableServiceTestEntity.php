<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mgo\DoctrineExtension\Annotation\Sequenceable;

/**
 * @ORM\Table(name="sequenceable_service")
 * @ORM\Entity(repositoryClass="Doctrine\ORM\EntityRepository")
 */
class SequenceableServiceTestEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @Sequenceable(service="test_sequenceable_formatter_service")
     * @ORM\Column(name="by_service", type="string", nullable=false)
     */
    private $byService;

    /**
     * @var string
     * @Sequenceable(service="test_sequenceable_formatter_service", grouping="groupId")
     * @ORM\Column(name="by_service_and_grouping", type="string", nullable=false)
     */
    private $byServiceAndGrouping;

    /**
     * @var string
     * @ORM\Column(name="groupId", type="string", nullable=false)
     */
    private $groupId;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set byService.
     *
     * @param string $byService
     *
     * @return self
     */
    public function setByService($byService)
    {
        $this->byService = $byService;

        return $this;
    }

    /**
     * Get byService.
     *
     * @return string
     */
    public function getByService()
    {
        return $this->byService;
    }

    /**
     * Set byService.
     *
     * @param string $byServiceAndGrouping
     *
     * @return self
     */
    public function setByServiceAndGrouping($byServiceAndGrouping)
    {
        $this->byServiceAndGrouping = $byServiceAndGrouping;

        return $this;
    }

    /**
     * Get ByServiceAndGrouping.
     *
     * @return string
     */
    public function getByServiceAndGrouping()
    {
        return $this->byServiceAndGrouping;
    }

    /**
     * Set groupId.
     *
     * @param string $groupId
     *
     * @return self
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Get groupId.
     *
     * @return string
     */
    public function getGroupId()
    {
        return $this->groupId;
    }
}
