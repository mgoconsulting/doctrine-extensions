<?php

namespace Mgo\DoctrineExtension\Util;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

class QueryUtil
{
    public const DEFAULT_PAGE_SIZE = 10;

    public static function applyPaginated(
        Query $query,
        \Closure $closure,
        array $options = []
    ): bool {
        $resolver = new OptionsResolver();
        // pagination size
        $resolver->setDefault('page_size', self::DEFAULT_PAGE_SIZE);
        $resolver->addAllowedTypes('page_size', 'integer');
        // use progress bar instance
        $resolver->setDefault('instantiate_progress_bar', null);
        $resolver->addAllowedTypes('instantiate_progress_bar', ['null', OutputInterface::class]);
        // set progress bar format
        $resolver->setDefault('progress_bar_format', null);
        $resolver->addAllowedTypes('progress_bar_format', ['null', 'string']);
        // pre page closure
        $resolver->setDefault('pre_page_closure', function () {
        });
        $resolver->addAllowedTypes('pre_page_closure', [\Closure::class]);
        // post page closure
        $resolver->setDefault('post_page_closure', function () {
        });
        $resolver->addAllowedTypes('post_page_closure', [\Closure::class]);
        // set Paginator fetchJoinCollection
        $resolver->setDefault('paginator_fetch_join_collection', true);
        $resolver->addAllowedTypes('paginator_fetch_join_collection', ['bool']);
        // resolve options
        $options = $resolver->resolve($options);

        // init paginated query
        $query->setFirstResult(0);
        $query->setMaxResults($options['page_size']);
        $paginator = new Paginator($query, $options['paginator_fetch_join_collection']);
        $count = $paginator->count();
        if (!$count) {
            return false;
        }

        $context = [
            'entity_manager' => $em = $query->getEntityManager(),
            'count' => $count,
            'progress_bar' => null,
        ];

        // init propgress bar (progress bar need Output in constructor)
        if ($options['instantiate_progress_bar'] instanceof OutputInterface) {
            $context['progress_bar'] = new ProgressBar(
                $options['instantiate_progress_bar'],
                $count
            );
            if ($options['progress_bar_format']) {
                $context['progress_bar']->setFormat($options['progress_bar_format']);
            }
            $context['progress_bar']->start();
        }

        $prePageClosure = $options['pre_page_closure'];
        $postPageClosure = $options['post_page_closure'];

        $pagesCount = ceil($count / $options['page_size']);
        for ($page = 0; $page < $pagesCount; $page++) {
            $prePageClosure($context);

            $firstResult = $page * $query->getMaxResults();
            $query->setFirstResult($firstResult);
            foreach ($paginator->getIterator() as $i => $item) {
                $context['page'] = $page + 1;
                $context['page_index'] = $i + 1;
                $context['index'] = ($page * $query->getMaxResults()) + $i + 1;
                // check callback options are valid (internal check only)
                $resolver = new OptionsResolver();
                // entity manager
                $resolver->setRequired('entity_manager');
                $resolver->addAllowedTypes('entity_manager', EntityManagerInterface::class);
                // total number of results
                $resolver->setRequired('count');
                $resolver->addAllowedTypes('count', 'integer');
                // page number
                $resolver->setRequired('page');
                $resolver->addAllowedTypes('page', 'integer');
                // page index of the current item
                $resolver->setRequired('page_index');
                $resolver->addAllowedTypes('page_index', 'integer');
                // total index of the current item
                $resolver->setRequired('index');
                $resolver->addAllowedTypes('index', 'integer');
                // total number of results
                $resolver->setDefault('progress_bar', null);
                $resolver->addAllowedTypes('progress_bar', ['null', ProgressBar::class]);
                // resolve callback options for each items
                $context = $resolver->resolve($context);

                $closure($item, $context);
                if ($context['progress_bar'] instanceof ProgressBar) {
                    $context['progress_bar']->advance();
                }
            }
            $postPageClosure($context);
        }

        return true;
    }
}
