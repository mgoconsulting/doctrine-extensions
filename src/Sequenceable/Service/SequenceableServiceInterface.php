<?php

namespace Mgo\DoctrineExtension\Sequenceable\Service;

use Doctrine\Persistence\ObjectManager;

interface SequenceableServiceInterface
{
    public function next(ObjectManager $em, object $object, string $field, array $configuration);
}
