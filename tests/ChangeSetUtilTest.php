<?php

namespace Test;

use Mgo\DoctrineExtension\Util\ChangeSetUtil;
use Test\Entity\ChangetSetEntity;

class ChangeSetUtilTest extends TestCase
{
    public function getChangeSetData()
    {
        return [
            'Change set strings' => [
                'aaaa',
                'bbbb',
            ],
            'Change set integers' => [
                '123',
                123,
            ],
            'Change set empty and null values' => [
                '',
                null,
            ],
        ];
    }

    /**
     * @dataProvider getChangeSetData
     */
    public function testGetChangeSet(
        $insertValue,
        $updateValue,
        array $options = []
    ) {
        /** @var \Doctrine\ORM\EntityManagerInterface $em */
        $em = self::$doctrine->getManagerForClass(ChangetSetEntity::class);

        $entity = new ChangetSetEntity();
        $entity->setName($insertValue);
        $em->persist($entity);

        $options['compute_changeset'] = true; // force compute_changeset pour le test
        $changeSetUtil = new ChangeSetUtil($em, $entity, $options);

        // full changeset
        $changeSet = $changeSetUtil->getChangeSet();

        // assert array length
        $this->assertIsArray($changeSet);
        $this->assertCount(1, $changeSet);
        // assert array keys
        $this->assertArrayHasKey('name', $changeSet);
        // assert array values
        $this->assertSame($changeSet['name'], [null, $insertValue]);

        // strict changeset
        $changeSet = $changeSetUtil->getChangeSet(true);

        // assert array length
        $this->assertIsArray($changeSet);
        $this->assertCount(1, $changeSet);

        // assert array keys
        $this->assertArrayHasKey('name', $changeSet);
        // assert array values
        $this->assertSame($changeSet['name'], [null, $insertValue]);

        $em->flush();

        // update entity
        $entity->setName($updateValue);
        $em->persist($entity);

        $changeSetUtil = new ChangeSetUtil($em, $entity, $options);

        // full changeset
        $changeSet = $changeSetUtil->getChangeSet();

        // assert array length
        $this->assertIsArray($changeSet);
        $this->assertCount(1, $changeSet);
        // assert array keys
        $this->assertArrayHasKey('name', $changeSet);
        // assert array values
        $this->assertSame($changeSet['name'], [$insertValue, $updateValue]);

        // strict changeset
        $changeSet = $changeSetUtil->getChangeSet(true);

        // assert array length
        $this->assertIsArray($changeSet);
        $this->assertCount(1, $changeSet);

        // assert array keys
        $this->assertArrayHasKey('name', $changeSet);
        // assert array values
        $this->assertSame($changeSet['name'], [$insertValue, $updateValue]);

        $em->remove($entity);
        $em->flush();
    }

    public function getChangeSetStrictWithCastMappingData()
    {
        return [
            'Cast string to integer' => [
                9999,
                '9999',
                ['string' => 'integer'],
            ],
            'Cast boolean to integer' => [
                1,
                true,
                ['string' => 'boolean'],
            ],
        ];
    }

    /**
     * @dataProvider getChangeSetStrictWithCastMappingData
     */
    public function testGetChangeSetStrictWithCastMapping(
        $insertValue,
        $updateValue,
        array $castMapping
    ) {
        /** @var \Doctrine\ORM\EntityManagerInterface $em */
        $em = self::$doctrine->getManagerForClass(ChangetSetEntity::class);

        $entity = new ChangetSetEntity();
        $entity->setName($insertValue); // integer
        $em->persist($entity);
        $em->flush();

        // update entity
        $entity->setName($updateValue);
        $em->persist($entity);

        $changeSetUtil = new ChangeSetUtil($em, $entity, [
            'compute_changeset' => true,
            'strict_type_cast_mapping' => $castMapping,
        ]);

        // full changeset
        $changeSet = $changeSetUtil->getChangeSet();

        // assert array length
        $this->assertIsArray($changeSet);
        $this->assertCount(1, $changeSet);
        // assert array keys
        $this->assertArrayHasKey('name', $changeSet);
        // assert array values
        $this->assertSame($changeSet['name'], [$insertValue, $updateValue]);

        // full strict changeset
        $changeSet = $changeSetUtil->getChangeSet(true);

        // assert array length
        $this->assertIsArray($changeSet);
        $this->assertCount(0, $changeSet);

        $em->remove($entity);
        $em->flush();
    }
}
