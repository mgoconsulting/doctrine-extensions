<?php

namespace Mgo\DoctrineExtension\Util;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangeSetUtil
{
    /** @var array */
    private $options;

    /** @var array */
    private $changeSet;

    /** @var array */
    private $strictChangeSet;

    private $metadata;

    public function __construct(EntityManagerInterface $em, object $object, array $options = [])
    {
        $this->options = $this->resolveOptions($options);

        $uow = $em->getUnitOfWork();
        if ($this->options['compute_changeset']) {
            $uow->computeChangeSets();
        }
        $this->changeSet = $uow->getEntityChangeSet($object);
        $this->metadata = $em->getMetadataFactory()->getMetadataFor(\get_class($object));
    }

    public function getChangeSet(bool $strict = false): array
    {
        if ($strict) {
            if (!is_array($this->strictChangeSet)) {
                $this->strictChangeSet = [];
                foreach ($this->changeSet as $name => $change) {
                    if (in_array($name, $this->options['disabled'])) {
                        continue;
                    }

                    $type = null;
                    if ($this->metadata->hasField($name) && $field = $this->metadata->getFieldMapping($name)) {
                        $type = $field['type'];
                    }

                    if ($type && isset($this->options['strict_type_cast_mapping'][$type])) {
                        $type = strtolower($this->options['strict_type_cast_mapping'][$type]);
                        settype($change[0], $type);
                        settype($change[1], $type);
                    }

                    if ($change[0] !== $change[1]) {
                        $this->strictChangeSet[$name] = $change;
                    }
                }
            }

            return $this->strictChangeSet;
        }

        return $this->changeSet;
    }

    public function hasChanged($name): bool
    {
        $this->strictChangeSet = $this->getChangeSet(true);

        return isset($this->strictChangeSet[$name]);
    }

    public function hasAtLeastOneChanged(array $names): bool
    {
        foreach ($names as $name) {
            if ($this->hasChanged($name)) {
                return true;
            }
        }

        return false;
    }

    private function resolveOptions($options): array
    {
        $resolver = new OptionsResolver();
        $resolver->setDefault('strict_type_cast_mapping', []);
        $resolver->setAllowedTypes('strict_type_cast_mapping', 'array');
        $resolver->setDefault('disabled', []);
        $resolver->setAllowedTypes('disabled', 'array');
        $resolver->setDefault('compute_changeset', false);
        $resolver->setAllowedTypes('compute_changeset', 'bool');

        return $resolver->resolve($options);
    }
}
