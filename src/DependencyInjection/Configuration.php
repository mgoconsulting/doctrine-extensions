<?php

namespace Mgo\DoctrineExtension\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Mgo\DoctrineExtension\Origin\Service\OriginService;

/**
 * Bundle Configuration.
 */
class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('mgo_doctrine_extension');
        // config component version
        if (\method_exists($treeBuilder, 'getRootNode')) {
            $root = $treeBuilder->getRootNode();
        } else {
            $root = $treeBuilder->root('mgo_doctrine_extension');
        }

        /** @var \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $root */
        $root
            ->children()
                ->booleanNode('sequenceable')
                    ->defaultFalse()
                ->end()
                ->booleanNode('dependson')
                    ->defaultFalse()
                ->end()
                ->arrayNode('origin')
                    ->canBeEnabled()
                    ->children()
                        ->scalarNode('format')
                            ->defaultValue('%%s@%%s') // double %% to avoid dependency parameters %blabla%
                            ->cannotBeEmpty()
                        ->end()
                        ->arrayNode('rules')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->arrayNode(OriginService::ORIGIN_CLI)
                                    ->scalarPrototype()->end()
                                ->end()
                                ->arrayNode(OriginService::ORIGIN_WEB)
                                    ->scalarPrototype()->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
