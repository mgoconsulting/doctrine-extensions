<?php

namespace Mgo\DoctrineExtension\Origin\Mapping\Driver;

use Gedmo\Exception\InvalidMappingException;
use Gedmo\Mapping\Driver\AbstractAnnotationDriver;
use Mgo\DoctrineExtension\Annotation\Origin;
use Symfony\Component\OptionsResolver\Exception\ExceptionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Annotation extends AbstractAnnotationDriver
{
    /**
     * {@inheritdoc}
     */
    public function readExtendedMetadata($meta, array &$config)
    {
        $class = $this->getMetaReflectionClass($meta);

        // property annotations
        foreach ($class->getProperties() as $property) {
            if ($origin = $this->reader->getPropertyAnnotation($property, Origin::class)) {
                $field = $property->getName();
                if (!$meta->hasField($field)) {
                    $message = \sprintf(
                        'Unable to find property %s::$%s for @origin',
                        $class,
                        $field
                    );
                    throw new InvalidMappingException($message);
                }

                if (!$origin->service) {
                    $message = \sprintf(
                        "None service is configured for annotation Origin in property %s::\$%s",
                        $class,
                        $field
                    );
                    throw new InvalidMappingException($message);
                }

                $config['origin']['fields'][$field] = [
                    'service' => $origin->service,
                ];
            }
        }
    }
}
