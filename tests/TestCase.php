<?php

namespace Test;

use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class TestCase extends WebTestCase
{
    /** @var \Doctrine\Persistence\ManagerRegistry */
    protected static $doctrine;

    /** @var ValidatorInterface */
    protected static $validator;

    public static function setUpBeforeClass(): void
    {
        // remove cache dir
        $filesystem = new Filesystem();
        $filesystem->remove(__DIR__ . '/../var/cache/test');

        // create kernel
        static::bootKernel(['debug' => self::isDebug()]);
        // get services
        self::$doctrine = self::$kernel->getContainer()->get('doctrine');
        self::$validator = self::$kernel->getContainer()->get('validator');
        // create db
        self::buildDb();
    }

    private static function buildDb(): void
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = self::$doctrine->getManager();
        $connection = $em->getConnection();
        $database = $connection->getDatabase();

        $schemaManager = $connection->getSchemaManager();
        $schemaManager->dropDatabase($database);
        $schemaManager->createDatabase($database);

        $schemaTool = new SchemaTool($em);
        $schemaTool->createSchema($em->getMetadataFactory()->getAllMetadata());
    }

    protected static function isDebug(): bool
    {
        return \in_array('--debug', $_SERVER['argv'] ?? []);
    }
}
