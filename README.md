# **MGO Doctrine Extensions Symfony Bundle**
# Doctrine Behavioral Extensions based on Gedmo

This package contains a Symfony Bundle with custom extensions for Doctrine ORM **ONLY** that offer new functionality or tools to use Doctrine
more efficiently. These behaviors can be easily attached to the event system of Doctrine and handle the records being
flushed in a behavioral way.


## Installation

#### Installation with composer
```sh
composer config repositories.mgoconsulting/doctrine-extensions git https://bitbucket.org/mgoconsulting/doctrine-extensions.git
composer require mgoconsulting/doctrine-extensions
```
#### Enable the bundle

In *config/bundles.php*
```php
<?php
return [
    ...
    Mgo\DoctrineExtension\DoctrineExtensionBundle::class => ['all' => true],
];
```
#### Enable the extension

In *config/packages/mgo_doctrine_extension.yaml*
```yaml
mgo_doctrine_extension:
    sequenceable: true  # enable or disable the sequenceable behaviour
    dependson: true     # enable or disable the dependson behaviour
    origin:
        enabled: true   # enable or disable the origin behaviour
        format: 'testing_%%s@%%s' # specific string format
        rules:
            cli: [] # cli rules
            web: [] # web rules  
```



## Sequenceable Behavior

**Sequenceable** behavior will automate a sequence for a field.

Features:

- Automatic sequence number
- ORM support using listener
- Specific annotations for properties, and no interface required
- Can react to specific property or relation changes to specific value
- Can be nested with other behaviors
- Annotation support ONLY *(TODO Yaml and Xml mapping)*

#### Sequenceable annotations:
- **@Mgo\DoctrineExtension\Annotation\Sequenceable;** this annotation tells that this column is sequenceable.
It updates this column on pre-persist.

Available configuration options:

- **grouping** - Grouping id to behave like a batch of sequences (default = NULL)
- **startAt** - sequences starts at this number (default = 0)
- **step** - sequences step *(e.g: 10, 20, 30 etc..)*

```php
<?php
namespace My\App;

use Mgo\DoctrineExtension\Annotation\Sequenceable as Mgo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class SequenceableExample
{
    /** @ORM\Id @ORM\GeneratedValue @ORM\Column(type="integer") */
    private $id;

    /**
     * Example with no options : 1, 2, 3, 4, 5, 6 etc...
     * @var string
     * @Mgo\Sequenceable()
     * @ORM\Column(name="sequence", type="integer", nullable=false)
     */
    private $sequence;

    /**
     * Example with step = 10 : 1, 11, 21, 31, 41 etc...
     * @var string
     * @Mgo\Sequenceable(step=10)
     * @ORM\Column(name="step", type="integer", nullable=false)
     */
    private $step;

    /**
     * Example with startAt = 50 : 50, 51, 52, 53, 54 etc...
     * @var string
     * @Mgo\Sequenceable(startAt=50)
     * @ORM\Column(name="startAt", type="integer", nullable=false)
     */
    private $startAt;

    /**
     * Example with grouping : reference to groupId and assoc field below
     * @var string
     * @Mgo\Sequenceable(grouping={"groupId", "assoc"})
     * @ORM\Column(name="grouped", type="integer", nullable=false)
     */
    private $grouped;

    /**
     * @var string
     * @ORM\Column(name="groupId", type="integer", nullable=false)
     */
    private $groupId;

    /**
     * @ORM\ManyToOne(targetEntity="My\App\SequenceableAssocExample")
     * @ORM\JoinColumn(name="test_id")
     */
    private $assoc;
}
```






## Origin Behavior

**Origin** behavior will save the context origin from a request or script.

Features:

- Automatic context guess.
- ORM support using listener
- Specific annotations for properties, and no interface required
- As a trait to use the defaut property
- Can be nested with other behaviors
- Annotation support ONLY *(TODO Yaml and Xml mapping)*

#### Origin annotations:
- **@Mgo\DoctrineExtension\Annotation\Origin;** this annotation tells that this column is for the origin.
It updates this column on pre-persist.

Available configuration options:

- **service** - To use your own service using the `OriginServiceInterface` (default = 'mgo.doctrine_extension.default_origin_service')

and in *config/packages/mgo_doctrine_extension.yaml* you can set your own custom rules:
```yaml
...
        rules:
            cli:
                # if matched, then will set origin = cli@phpunit
                phpunit: 'argv[0] matches "#phpunit#"'
                # if matched, then will set origin = cli@symfony_command
                symfony_command: 'argv[0] matches "#bin/console#"'
            web:
                # if matched, then will set origin = web@api
                api: 'request.getPathInfo() matches "#^/api/#"'
```


```php
<?php
namespace My\App;

use Doctrine\ORM\Mapping as ORM;
use Mgo\DoctrineExtension\Origin\Traits\OriginableEntity;

/**
 * @ORM\Entity
 */
class OriginExample
{
    // use the trait
    use OriginableEntity;
    
    /** @ORM\Id @ORM\GeneratedValue @ORM\Column(type="integer") */
    private $id;
}
```






## Depends On Validator

The purpose of the ServicedCallback constraint is to create completely custom validation rules and inject services and parameters at the same time.

```php
<?php
namespace App\Entity;

use Mgo\DoctrineExtension\Validator\ServicedCallback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class Author
{
    /**
     * @ServicedCallback(services={"doctrine", "my_custom_services"}, parameters={"kernel.environment"})
     */
    public function validate(ExecutionContextInterface $context, $payload, $doctrine, $customService, $env)
    {
        $doctrine; // doctrine is accessible now
        $customService; // my_custom_services is accessible too
        $env; // parameter environment is accessible too
    }
}
```



## Post Flush Subscriber Abstraction

The **AbstractPostFlushSubscriber** abstraction class will help to hook on the doctrine flush event.

```php
<?php

namespace App\EventListener;

use Mgo\DoctrineExtension\EventListenerUtil\AbstractPostFlushSubscriber;
use Test\Entity\MyEntity;

class ExemplePostFlushSubscriber extends AbstractPostFlushSubscriber
{
    protected function getSubscribedClasses(): array
    {
        return [EventTestEntity::class];
    }

    protected function doPostFlush(object $entity, EntityManagerInterface $em): void
    {
        // do stuff
    }
}
```




## Query Util

The **QueryUtil** will help with many query use cases.

#### Apply pagination
```php

use Mgo\DoctrineExtension\Util\QueryUtil;

// super big query
$qb = $repo->createQueryBuilder('c');
$qb->andWhere('c.blabla IS NOT NULL');

QueryUtil::applyPaginated(
    $qb,
    function ($item, $context) {
        // do stuff for each results
        $item; // the result item
        $context; // array with context iteration details (total count, indexes, entity_manager, progress bar ... )
    },
    // options
    [
        // page size for each DB queries
        'page_size' => 10,
        // instantiate  console progress bar (needs a OutputInterface instance)
        'instantiate_progress_bar' => $output,
        'progress_bar_format' => 'your custom progress bar output format',
    ]
);
```
