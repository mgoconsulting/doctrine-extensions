<?php

namespace Mgo\DoctrineExtension\Origin\Service;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\ExpressionLanguage\SyntaxError;

class OriginService implements OriginServiceInterface
{
    public const ORIGIN_CLI = 'cli';
    public const ORIGIN_WEB = 'web';

    /** @var array */
    private $config = [];

    /** @var \Symfony\Component\HttpFoundation\Request|null */
    private $request;

    public function __construct(array $config, RequestStack $requestStack)
    {
        $this->config = $config;
        // check for getMasterRequest which is deprecated since symfony/http-foundation 5.3
        if (\is_callable([$requestStack, 'getMainRequest'])) {
            $this->request = $requestStack->getMainRequest();
        } else { // @phpstan-ignore-line
            $this->request = $requestStack->getMasterRequest();
        }
    }

    public function getOriginContext(): ?string
    {
        if (self::ORIGIN_CLI === php_sapi_name()) {
            return $this->handleCli();
        }

        if ($this->request) {
            return $this->handleWeb();
        }

        // did not find any context to match
        return null;
    }

    private function handleCli(): string
    {
        $rules = $this->config['rules'][self::ORIGIN_CLI];
        $argv = $_SERVER['argv'] ?? [];

        // search for symfony command if exists, and try to find it
        $symfonyCommand = false;
        if (preg_grep("#bin/console#", $argv)) {
            // try to match blabl:blabla in symfony command
            $matches = preg_grep("#[^:]+:[^:]+#", $argv);
            if (1 === \count($matches)) {
                $symfonyCommand = current($matches);
            }
        }

        return $this->checkRules(
            self::ORIGIN_CLI,
            $rules,
            ['argv' => $argv, 'sf_command' => $symfonyCommand]
        );
    }

    private function handleWeb(): string
    {
        $rules = $this->config['rules'][self::ORIGIN_WEB];

        return $this->checkRules(
            self::ORIGIN_WEB,
            $rules,
            ['request' => $this->request]
        );
    }

    private function checkRules(string $context, array $rules, array $expressionValues): string
    {
        $el = new ExpressionLanguage();
        foreach ($rules as $name => $expression) {
            try {
                $value = $el->evaluate($expression, $expressionValues);
            } catch (SyntaxError $e) {
                // throw just a warning
                @trigger_error('Origin CLI rules syntax error: ' . $e->getMessage(), E_USER_WARNING);
                continue;
            }
            if ($value) {
                return \sprintf($this->config['format'], $context, (\is_string($value) ? $value : $name));
            }
        }

        return $context;
    }
}
