<?php

namespace Mgo\DoctrineExtension\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * DependsOn annotation.
 *
 * @Annotation
 * @Target("PROPERTY")
 */
final class DependsOn extends Annotation
{
}
