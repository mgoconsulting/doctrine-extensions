<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="change_set")
 * @ORM\Entity(repositoryClass="Doctrine\ORM\EntityRepository")
 */
class ChangetSetEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }
}
