<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mgo\DoctrineExtension\Annotation\DependsOn;

/**
 * @ORM\Table(name="Depends_on")
 * @ORM\Entity(repositoryClass="Doctrine\ORM\EntityRepository")
 */
class DependsOnTestEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @DependsOn({"b": {"operator": "xor"}})
     * @ORM\Column(type="integer", nullable=true)
     */
    private $a;

    /**
     * @var string
     * @ORM\Column(type="integer", nullable=true)
     */
    private $b;

    /**
     * @var string
     * @DependsOn({"d": {"operator": "and"}})
     * @ORM\Column(type="integer", nullable=true)
     */
    private $c;

    /**
     * @var string
     * @ORM\Column(name="d", type="integer", nullable=true)
     */
    private $d;

    /**
     * @var string
     * @DependsOn({"f": {"operator": "or"}})
     * @ORM\Column(type="integer", nullable=true)
     */
    private $e;

    /**
     * @var string
     * @ORM\Column(type="integer", nullable=true)
     */
    private $f;

    /**
     * @var string
     * @DependsOn({"h": {"operator": "not_and"}})
     * @ORM\Column(type="integer", nullable=true)
     */
    private $g;

    /**
     * @var string
     * @ORM\Column(type="integer", nullable=true)
     */
    private $h;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setA($a)
    {
        $this->a = $a;

        return $this;
    }

    public function getA()
    {
        return $this->a;
    }

    public function setB($b)
    {
        $this->b = $b;

        return $this;
    }

    public function getB()
    {
        return $this->b;
    }

    public function getC()
    {
        return $this->c;
    }

    public function setC($c)
    {
        $this->c = $c;

        return $this;
    }

    public function getD()
    {
        return $this->d;
    }

    public function setD($d)
    {
        $this->d = $d;

        return $this;
    }

    public function getE()
    {
        return $this->e;
    }

    public function setE($e)
    {
        $this->e = $e;

        return $this;
    }

    public function getF()
    {
        return $this->f;
    }

    public function setF($f)
    {
        $this->f = $f;

        return $this;
    }

    public function getG()
    {
        return $this->g;
    }

    public function setG($g)
    {
        $this->g = $g;

        return $this;
    }

    public function getH()
    {
        return $this->h;
    }

    public function setH($h)
    {
        $this->h = $h;

        return $this;
    }
}
