<?php

namespace Mgo\DoctrineExtension\Sequenceable;

use Doctrine\Common\EventArgs;
use Doctrine\ORM\Event;
use Doctrine\ORM\Events;
use Gedmo\Mapping\MappedEventSubscriber;
use Mgo\DoctrineExtension\Sequenceable\Service\SequenceableServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

class SequenceableListener extends MappedEventSubscriber
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    public function getSubscribedEvents()
    {
        return [Events::prePersist, Events::loadClassMetadata];
    }

    public function loadClassMetadata(Event\LoadClassMetadataEventArgs $eventArgs)
    {
        $ea = $this->getEventAdapter($eventArgs);
        $this->loadMetadataForObjectClass($ea->getObjectManager(), $eventArgs->getClassMetadata());
    }

    public function prePersist(Event\LifecycleEventArgs $args)
    {
        $ea = $this->getEventAdapter($args);
        $em = $ea->getObjectManager();
        $object = $ea->getObject();
        $meta = $em->getClassMetadata($class = get_class($object));

        if ($config = $this->getConfiguration($em, $meta->getName())) {
            if (!isset($config['sequenceable'])) {
                return;
            }

            // query builder helper
            $helper = new SequenceableHelper($em);
            // get PropertyAccessor
            $pa = SequenceableHelper::getPropertyAccessor();

            // for each field with !Sequenceable annotation
            foreach ($config['sequenceable']['fields'] as $field => $conf) {
                // check service first
                if ($conf['service']) {
                    if (!$this->container->has($conf['service'])) {
                        $message = sprintf(
                            "Sequenceable service '%s' defined in %s::%s was not found or not set as public",
                            $conf['service'],
                            $class,
                            $field
                        );
                        throw new \RuntimeException($message);
                    }
                    $service = $this->container->get($conf['service']);

                    if (!$service instanceof SequenceableServiceInterface) {
                        $message = sprintf(
                            "Sequenceable service '%s' (%s) defined in %s::%s does not implement interface %s",
                            $conf['service'],
                            \get_class($service),
                            $class,
                            $field,
                            SequenceableServiceInterface::class
                        );
                        throw new \RuntimeException($message);
                    }

                    $pa->setValue(
                        $object,
                        $field,
                        $service->next($em, $object, $field, $conf)
                    );
                    continue;
                }

                $qb = $helper->getLastItemOfSequenceQueryBuilder($object, $field, $conf);

                // find last element in the sequence
                $res = $qb->getQuery()->getResult();

                // get last numero
                $newValue = $conf['start_at'] ?? 1;
                if (is_array($res) && 1 === count($res)) {
                    $lastValue = $pa->getValue($res[0], $field);
                    $newValue = $lastValue + ($conf['step'] ?? 1);
                }
                // set new value
                $pa->setValue($object, $field, $newValue);
            }
        }
    }

    protected function getNamespace()
    {
        return __NAMESPACE__;
    }
}
