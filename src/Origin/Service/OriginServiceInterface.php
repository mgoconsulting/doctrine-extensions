<?php

namespace Mgo\DoctrineExtension\Origin\Service;

use Doctrine\Persistence\ObjectManager;

interface OriginServiceInterface
{
    public function getOriginContext(): ?string;
}
