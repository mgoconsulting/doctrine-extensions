<?php

namespace Mgo\DoctrineExtension\DependsOn;

use Doctrine\ORM\Event;
use Doctrine\ORM\Events;
use Gedmo\Mapping\MappedEventSubscriber;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContext;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DependsOnListener extends MappedEventSubscriber
{
    /** @var array */
    private $loaded = [];

    /** @var ValidatorInterface */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        parent::__construct();
        $this->validator = $validator;
    }

    public function getSubscribedEvents()
    {
        return [Events::loadClassMetadata];
    }

    public function loadClassMetadata(Event\LoadClassMetadataEventArgs $eventArgs)
    {
        $ea = $this->getEventAdapter($eventArgs);
        $em = $ea->getObjectManager();
        $meta = $eventArgs->getClassMetadata();

        // Avoid loop on loadClassMetadata event
        $className = $meta->getName();
        if (isset($this->loaded[$className])) {
            return;
        }
        $this->loaded[$className] = true;

        if ($config = $this->getConfiguration($em, $className)) {
            if (!isset($config['dependson'])) {
                return;
            }

            /** @var \Symfony\Component\Validator\Mapping\ClassMetadata $validatorMeta */
            $validatorMeta = $this->validator->getMetadataFor($className);
            $validatorMeta->addConstraint(
                new Assert\Callback(function (object $entity, ExecutionContext $context) use ($config) {
                    $pa = PropertyAccess::createPropertyAccessorBuilder()
                        ->enableExceptionOnInvalidIndex()
                        ->getPropertyAccessor();
                    $el = new ExpressionLanguage();

                    // for each field with @DependsOn annotation
                    foreach ($config['dependson']['fields'] as $source => $dependencies) {
                        foreach ($dependencies as $depFieldName => $conf) {
                            $prepend = null; // optional prepend expression
                            $operator = ($conf['operator'] ?? null);
                            $message = null;
                            switch ($operator) {
                                case 'and':
                                    $message = 'both should be set';
                                    break;
                                case 'or':
                                    $message = 'at least one of these fields should be set';
                                    break;
                                case 'xor':
                                    $operator = '^';
                                    $message = 'One or the other should be set, not both';
                                    break;
                                case 'not_and':
                                    $operator = 'and';
                                    $prepend = 'not';
                                    $message = 'Not both fields can be set';
                                    break;
                            }
                            $valid = (bool) $el->evaluate(
                                // using not(not()) to cast values to boolean
                                "{$prepend}((value !== false_value) {$operator} (dep_value !== false_value))",
                                [
                                    'value' => $pa->getValue($entity, $source),
                                    'dep_value' => $pa->getValue($entity, $depFieldName),
                                    'false_value' => null, // value that defines false state bitwise
                                ]
                            );
                            if (!$valid) {
                                $message = \sprintf(
                                    '"%s" field depends on "%s": %s.',
                                    $depFieldName,
                                    $source,
                                    $message
                                );
                                $context->buildViolation($message)
                                    ->atPath($depFieldName)
                                    ->addViolation();
                            }
                        }
                    }
                })
            );
        }
    }

    protected function getNamespace()
    {
        return __NAMESPACE__;
    }
}
