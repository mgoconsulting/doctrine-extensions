<?php

namespace Test;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

// phpcs:disable
require __DIR__.'/../vendor/autoload.php';
// phpcs:enable

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public function registerBundles()
    {
        return [
            new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new \Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new \Mgo\DoctrineExtension\DoctrineExtensionBundle(),
        ];
    }

    protected function configureContainer(ContainerBuilder $builder, LoaderInterface $loader)
    {
        $dbPath = sys_get_temp_dir() . '/mgo-doctrine-extension-test.db';
        @unlink($dbPath);

        $builder->loadFromExtension('framework', [
            'secret' => 'TEST',
            'test' => true,
        ]);
        $builder->loadFromExtension('doctrine', [
            'dbal' => [
                'default_connection' => 'test',
                'connections' => [
                    'test' => [
                        'driver' => 'pdo_sqlite',
                        'path' => $dbPath,
                    ],
                ],
            ],
            'orm' => [
                'mappings' => [
                    'test' => [
                        'type' => 'annotation',
                        'prefix' => 'Test\Entity',
                        'dir' => __DIR__ . '/Entity',
                    ],
                ],
            ],
        ]);
        $builder->loadFromExtension('mgo_doctrine_extension', [
            'sequenceable' => true,
            'dependson' => true,
            'origin' => [
                'format' => 'testing_%%s@%%s', // double %% to avoid dependency parameters %blabla%
                'rules' => [ // NOTE argv has custom values in tests
                    'cli' => [
                        'phpunit' => 'argv[0] matches "/phpunit/"',
                        'custom' => 'argv[0] matches "/custom/" ? "custom_string" : false',
                        'symfony_command' => 'sf_command',
                    ],
                ]
            ],
        ]);

        // load test services
        $loader->load(__DIR__ . '/services.yml');
    }
}
