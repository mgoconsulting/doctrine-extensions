<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mgo\DoctrineExtension\Annotation\Sequenceable;

/**
 * @ORM\Table(name="sequenceable")
 * @ORM\Entity(repositoryClass="Doctrine\ORM\EntityRepository")
 */
class SequenceableTestEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @Sequenceable()
     * @ORM\Column(name="numero", type="integer", nullable=false)
     */
    private $numero;

    /**
     * @var int
     * @Sequenceable(step=10)
     * @ORM\Column(name="step", type="integer", nullable=false)
     */
    private $step;

    /**
     * @var int
     * @Sequenceable(startAt=50)
     * @ORM\Column(name="startAt", type="integer", nullable=false)
     */
    private $startAt;

    /**
     * @var int
     * @Sequenceable(grouping="groupId")
     * @ORM\Column(name="grouped", type="integer", nullable=false)
     */
    private $grouped;

    /**
     * @var int
     * @Sequenceable(grouping="groupId", startAt=0)
     * @ORM\Column(name="groupedStartAt", type="integer", nullable=false)
     */
    private $groupedStartAt;

    /**
     * @var int
     * @Sequenceable(grouping="groupId", step=10, startAt=10)
     * @ORM\Column(name="groupedStepStartAt", type="integer", nullable=false)
     */
    private $groupedStepStartAt;

    /**
     * @var int
     * @Sequenceable(grouping={"groupId", "otherGroupId"})
     * @ORM\Column(name="groupedMany", type="integer", nullable=false)
     */
    private $groupedMany;

    /**
     * @var int
     * @ORM\Column(name="groupId", type="integer", nullable=false)
     */
    private $groupId;

    /**
     * @var int
     * @ORM\Column(name="otherGroupId", type="integer", nullable=false)
     */
    private $otherGroupId;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero.
     *
     * @param int $numero
     *
     * @return self
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero.
     *
     * @return int
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set step.
     *
     * @param int $step
     *
     * @return self
     */
    public function setStep($step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * Get step.
     *
     * @return int
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * Set startAt.
     *
     * @param int $startAt
     *
     * @return self
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Get startAt.
     *
     * @return int
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * Set grouped.
     *
     * @param int $grouped
     *
     * @return self
     */
    public function setGrouped($grouped)
    {
        $this->grouped = $grouped;

        return $this;
    }

    /**
     * Get grouped.
     *
     * @return int
     */
    public function getGrouped()
    {
        return $this->grouped;
    }

    /**
     * Set groupedStartAt.
     *
     * @param int $groupedStartAt
     *
     * @return self
     */
    public function setGroupedStartAt($groupedStartAt)
    {
        $this->groupedStartAt = $groupedStartAt;

        return $this;
    }

    /**
     * Get groupedStartAt.
     *
     * @return int
     */
    public function getGroupedStartAt()
    {
        return $this->groupedStartAt;
    }

    /**
     * Set groupedMany.
     *
     * @param int $groupedMany
     *
     * @return self
     */
    public function setGroupedMany($groupedMany)
    {
        $this->groupedMany = $groupedMany;

        return $this;
    }

    /**
     * Get groupedMany.
     *
     * @return int
     */
    public function getGroupedMany()
    {
        return $this->groupedMany;
    }

    /**
     * Set groupedStepStartAt.
     *
     * @param int $groupedStepStartAt
     *
     * @return self
     */
    public function setGroupedStepStartAt($groupedStepStartAt)
    {
        $this->groupedStepStartAt = $groupedStepStartAt;

        return $this;
    }

    /**
     * Get groupedStepStartAt.
     *
     * @return int
     */
    public function getGroupedStepStartAt()
    {
        return $this->groupedStepStartAt;
    }

    /**
     * Set groupId.
     *
     * @param int $groupId
     *
     * @return self
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Get groupId.
     *
     * @return int
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Set otherGroupId.
     *
     * @param int $otherGroupId
     *
     * @return self
     */
    public function setOtherGroupId($otherGroupId)
    {
        $this->otherGroupId = $otherGroupId;

        return $this;
    }

    /**
     * Get otherGroupId.
     *
     * @return int
     */
    public function getOtherGroupId()
    {
        return $this->otherGroupId;
    }
}
