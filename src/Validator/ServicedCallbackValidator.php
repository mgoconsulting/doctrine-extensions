<?php

namespace Mgo\DoctrineExtension\Validator;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ServicedCallbackValidator extends ConstraintValidator
{
    use ContainerAwareTrait;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($object, Constraint $constraint)
    {
        if (!$constraint instanceof ServicedCallback) {
            throw new UnexpectedTypeException($constraint, ServicedCallback::class);
        }

        $args = [$this->context, $constraint->payload];
        foreach ((array) $constraint->services as $service) {
            if (!$this->container->has($service)) {
                throw new InvalidOptionsException("Service '{$service}' not found", (array) $constraint->services);
            }
            $args[] = $this->container->get($service);
        }
        foreach ((array) $constraint->parameters as $parameter) {
            if (!$this->container->hasParameter($parameter)) {
                throw new InvalidOptionsException(
                    "Parameter '{$parameter}' not found",
                    (array) $constraint->parameters
                );
            }
            $args[] = $this->container->getParameter($parameter);
        }

        $method = $constraint->callback;
        if ($method instanceof \Closure) {
            /** @var \Closure $method */
            \call_user_func_array($object, $args);
        } elseif (\is_array($method)) {
            /** @var array $method */
            if (!\is_callable($method)) {
                if (isset($method[0]) && \is_object($method[0])) {
                    $method[0] = \get_class($method[0]);
                }
                $message = json_encode($method) . ' targeted by Callback constraint is not a valid callable.';
                throw new ConstraintDefinitionException($message);
            }

            \call_user_func_array($method, $args);
        } elseif (null !== $object) {
            if (!method_exists($object, $method)) {
                $message = sprintf(
                    'Method "%s" targeted by Callback constraint does not exist in class "%s".',
                    $method,
                    \get_class($object)
                );
                throw new ConstraintDefinitionException($message);
            }

            $reflMethod = new \ReflectionMethod($object, $method);

            if ($reflMethod->isStatic()) {
                $reflMethod->invoke(null, \array_merge([$object], $args));
            } else {
                $reflMethod->invokeArgs($object, $args);
            }
        }
    }
}
