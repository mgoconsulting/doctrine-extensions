<?php

namespace Mgo\DoctrineExtension\Origin\Traits;

use Doctrine\ORM\Mapping as ORM;
use Mgo\DoctrineExtension\Annotation\Origin;

trait OriginableEntity
{
    /**
     * @var string
     * @Origin()
     * @ORM\Column(type="string", nullable=true)
     */
    private $contextOrigin;

    public function getContextOrigin()
    {
        return $this->contextOrigin;
    }

    public function setContextOrigin(string $contextOrigin)
    {
        $this->contextOrigin = $contextOrigin;

        return $this;
    }
}
