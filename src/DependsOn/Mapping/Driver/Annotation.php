<?php

namespace Mgo\DoctrineExtension\DependsOn\Mapping\Driver;

use Gedmo\Exception\InvalidMappingException;
use Gedmo\Mapping\Driver\AbstractAnnotationDriver;
use Mgo\DoctrineExtension\Annotation\DependsOn;
use Symfony\Component\OptionsResolver\Exception\ExceptionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Annotation extends AbstractAnnotationDriver
{
    /**
     * {@inheritdoc}
     */
    public function readExtendedMetadata($meta, array &$config)
    {
        $class = $this->getMetaReflectionClass($meta);

        // property annotations
        foreach ($class->getProperties() as $property) {
            if ($dependsOn = $this->reader->getPropertyAnnotation($property, DependsOn::class)) {
                $field = $property->getName();
                if (!$meta->hasField($field)) {
                    $message = \sprintf(
                        'Unable to find property %s::$%s for @dependsOn',
                        $class,
                        $field
                    );
                    throw new InvalidMappingException($message);
                }
                foreach ((array) $dependsOn->value as $propertyName => $depends) {
                    if (!$meta->hasField($propertyName)) {
                        $message = \sprintf(
                            "Unable to find dependsOn field '%s' for property %s::\$%s",
                            $propertyName,
                            $class,
                            $field
                        );
                        throw new InvalidMappingException($message);
                    }
                    $resolver = new OptionsResolver();
                    $resolver->setRequired('operator');
                    $resolver->setAllowedValues('operator', ['and', 'or', 'xor', 'not_and']);
                    try {
                        $depends = $resolver->resolve((array) $depends);
                    } catch (ExceptionInterface $e) {
                        $message = \sprintf(
                            'Invalid dependsOn option for property %s::$%s: %s',
                            $class,
                            $propertyName,
                            $e->getMessage()
                        );
                        throw new InvalidMappingException($message);
                    }
                    $config['dependson']['fields'][$field][$propertyName] = $depends;
                }
            }
        }
    }
}
