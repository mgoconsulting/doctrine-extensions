<?php

namespace Mgo\DoctrineExtension;

use Mgo\DoctrineExtension\DependencyInjection\MgoDoctrineExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * MGO Doctrine Extension Bundle.
 */
class DoctrineExtensionBundle extends Bundle
{
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = $this->createContainerExtension();
        }

        return $this->extension;
    }

    protected function getContainerExtensionClass()
    {
        return MgoDoctrineExtension::class;
    }
}
