<?php

namespace Mgo\DoctrineExtension\Origin;

use Doctrine\ORM\Event;
use Doctrine\ORM\Events;
use Gedmo\Mapping\MappedEventSubscriber;
use Mgo\DoctrineExtension\Origin\Service\OriginServiceInterface;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess as PA;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContext;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OriginListener extends MappedEventSubscriber
{
    use ContainerAwareTrait;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    public function getSubscribedEvents()
    {
        return [Events::prePersist, Events::loadClassMetadata];
    }

    public function loadClassMetadata(Event\LoadClassMetadataEventArgs $eventArgs)
    {
        $ea = $this->getEventAdapter($eventArgs);
        $this->loadMetadataForObjectClass($ea->getObjectManager(), $eventArgs->getClassMetadata());
    }

    public function prePersist(Event\LifecycleEventArgs $args)
    {
        $ea = $this->getEventAdapter($args);
        $em = $ea->getObjectManager();
        $object = $ea->getObject();
        $meta = $em->getClassMetadata($class = get_class($object));

        if ($config = $this->getConfiguration($em, $meta->getName())) {
            if (!isset($config['origin'])) {
                return;
            }

            $pa = PA\PropertyAccess::createPropertyAccessorBuilder()
                ->enableExceptionOnInvalidIndex()
                ->getPropertyAccessor();

            // for each field with !Sequenceable annotation
            foreach ($config['origin']['fields'] as $field => $conf) {
                if (!$this->container->has($conf['service'])) {
                    $message = sprintf(
                        "Origin service '%s' defined in %s::%s was not found or not set as public",
                        $conf['service'],
                        $class,
                        $field
                    );
                    throw new \RuntimeException($message);
                }
                $service = $this->container->get($conf['service']);

                if (!$service instanceof OriginServiceInterface) {
                    $message = sprintf(
                        "Origin service '%s' (%s) defined in %s::%s does not implement interface %s",
                        $conf['service'],
                        \get_class($service),
                        $class,
                        $field,
                        OriginServiceInterface::class
                    );
                    throw new \RuntimeException($message);
                }

                $pa->setValue(
                    $object,
                    $field,
                    $service->getOriginContext()
                );
            }
        }
    }

    protected function getNamespace()
    {
        return __NAMESPACE__;
    }
}
