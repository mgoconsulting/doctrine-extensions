<?php

namespace Test;

use Test\Entity\SequenceableServiceTestEntity;
use Test\Entity\SequenceableTestEntity;
use Test\Service\FormatterSequenceableService;

class SequenceableTest extends TestCase
{
    public function dataWithoutService()
    {
        // data order is important !
        return [
            '1st set' => [
                1,  // expected numero
                1,  // expected step
                50, // expected startAt
                1,  // expected grouped
                0,  // expected groupedStartAt
                10, // expected groupedStepStartAt
                1,  // expected groupedMany
                    // optional group id
                    // optional other group id
            ],
            '2nd set' => [2, 11, 51, 2, 1, 20, 2],
            '3rd set' => [3, 21, 52, 3, 2, 30, 3],
            '4th set' => [4, 31, 53, 4, 3, 40, 4],
            '5th set' => [5, 41, 54, 1, 0, 10, 1, 111], // new group 111 ($grouped starts from 1)
            '6th set' => [6, 51, 55, 1, 0, 10, 1, 222], // new group 222 ($grouped starts from 1)
            '7th set' => [7, 61, 56, 2, 1, 20, 2, 111],
            '8th set' => [8, 71, 57, 5, 4, 50, 5], // default group 1
            '9th set' => [9, 81, 58, 2, 1, 20, 2, 222],
            '10th set' => [10, 91, 59, 6, 5, 60, 6],
            '11th set' => [11, 101, 60, 3, 2, 30, 3, 222],
            '12th set' => [12, 111, 61, 3, 2, 30, 3, 111],
            '13th set' => [13, 121, 62, 1, 0, 10, 1, 999, 999], // group and other group
            '14th set' => [14, 131, 63, 2, 1, 20, 2, 999, 999], // group and other group again
        ];
    }

    /**
     * @dataProvider dataWithoutService
     */
    public function testWithoutService(
        $numero,
        $step,
        $startAt,
        $grouped,
        $groupedStartAt,
        $groupedStepStartAt,
        $groupedMany,
        $groupId = 1,
        $otherGroupId = 2
    ) {
        $entity = new SequenceableTestEntity();
        $this->assertNull($entity->getNumero(), 'numero before flush');
        $this->assertNull($entity->getStep(), 'step before flush');
        $this->assertNull($entity->getStartAt(), 'startAt before flush');
        $this->assertNull($entity->getGrouped(), 'grouped before flush');
        $this->assertNull($entity->getGroupedStartAt(), 'groupedStartAt before flush');
        $this->assertNull($entity->getGroupedStepStartAt(), 'groupedStepStartAt before flush');
        $this->assertNull($entity->getGroupedMany(), 'groupedMany before flush');
        $entity->setGroupId($groupId);
        $entity->setOtherGroupId($otherGroupId);
        /** @var \Symfony\Component\Validator\ConstraintViolationList $errors */
        $errors = self::$validator->validate($entity);
        $this->assertCount(0, $errors, (string) $errors);
        $em = self::$doctrine->getManagerForClass(SequenceableTestEntity::class);
        $em->persist($entity);
        $em->flush();
        $this->assertEquals($numero, $entity->getNumero(), 'numero after flush');
        $this->assertEquals($step, $entity->getStep(), 'step after flush');
        $this->assertEquals($startAt, $entity->getStartAt(), 'startAt after flush');
        $this->assertEquals($grouped, $entity->getGrouped(), 'grouped after flush');
        $this->assertEquals($groupedStartAt, $entity->getGroupedStartAt(), 'groupedStartAt after flush');
        $this->assertEquals($groupedStepStartAt, $entity->getGroupedStepStartAt(), 'groupedStepStartAt after flush');
        $this->assertEquals($groupedMany, $entity->getGroupedMany(), 'groupedMany after flush');
    }

    public function dataWithServiceFailures()
    {
        return [
            'invalid format too much %' => [
                'Impossible to generate sequence from "FAIL - %s %s".' . PHP_EOL .
                PHP_MAJOR_VERSION > 7
                ?
                "3 arguments are required, 2 given" // error for PHP 8
                :
                "Maybe the format has too much \"sprintf\" (%x) arguments ?",// error for PHP 7
                "\F\A\I\L - \%\s \%\s",
            ],
        ];
    }

    /**
     * @dataProvider dataWithServiceFailures
     */
    public function testWithServiceFailures(
        string $exceptionMessage,
        string $format
    ) {
        $this->expectExceptionMessage($exceptionMessage);
        FormatterSequenceableService::setFormat($format);
        $entity = new SequenceableServiceTestEntity();
        $errors = self::$validator->validate($entity);
        /** @var \Symfony\Component\Validator\ConstraintViolationList $errors*/
        $this->assertCount(0, $errors, (string) $errors);
        $em = self::$doctrine->getManagerForClass(SequenceableServiceTestEntity::class);
        $entity->setGroupId(\uniqid('whatever'));
        $em->persist($entity);
        $em->flush();
    }

    public function dataWithService()
    {
        $today = date('Y-m-d');

        $formats = [
            // simple format 1,2,3 etc ...
            '\%\d' => [
                'expectedWithoutGrouping' => '%2$d',
                'expectedWithGrouping' => '%2$d',
            ],
            // format with prefix AAAA: AAAA1, AAAA2, AAAA3 etc ...
            '\A\A\A\A\%\d' => [
                'expectedWithoutGrouping' => 'AAAA%2$d',
                'expectedWithGrouping' => 'AAAA%2$d',
            ],
            // format with suffix BBBB: 1BBBB, 2BBBB, 3BBBB etc ...
            '%\d\B\B\B\B' => [
                'expectedWithoutGrouping' => '%2$dBBBB',
                'expectedWithGrouping' => '%2$dBBBB',
            ],
            // format FA-2021-05-24-1, FA-2021-05-24-2 etc ...
            '\F\A-Y-m-d-\%\d' => [
                'expectedWithoutGrouping' => 'FA-%1$s-%2$d',
                'expectedWithGrouping' => 'FA-%1$s-%2$d',
                'dateFormat' => 'Y-m-d',
            ],
            // format with spaces and numbers 156155, 156255, 156355 etc ...
            '156\%\d55' => [
                'expectedWithoutGrouping' => '156%2$d55',
                'expectedWithGrouping' => '156%2$d55',
            ],
        ];

        $data = [];
        foreach ($formats as $format => $expectedValues) {
            $today = date($expectedValues['dateFormat'] ?? '');
            $groupingMap = [];
            for ($i = 1; $i < 120; $i++) { // $i > 9 to test 3 digits
                $grouping = ($i % 3) + 1; // 3 groups
                if (!\array_key_exists($grouping, $groupingMap)) {
                    $groupingMap[$grouping] = 0;
                }
                $groupingMap[$grouping]++;
                $data["Format {$format} - Set {$i}"] = [
                    $format,
                    $grouping,
                    sprintf($expectedValues['expectedWithoutGrouping'], $today, $i),
                    sprintf($expectedValues['expectedWithGrouping'], $today, $groupingMap[$grouping]),
                ];
            }
        }

        // data order is important !
        return $data;
    }

    /**
     * @dataProvider dataWithService
     */
    public function testWithServiceBothGroupingOrNot(
        string $format,
        int $grouping,
        string $expectedWithoutGrouping,
        string $expectedWithGrouping
    ) {
        FormatterSequenceableService::setFormat($format);
        $entity = new SequenceableServiceTestEntity();
        $this->assertNull($entity->getByService(), 'byService before flush');
        $this->assertNull($entity->getByServiceAndGrouping(), 'ByServiceAndGrouping before flush');
        $this->assertNull($entity->getGroupId(), 'group id before flush');
        $errors = self::$validator->validate($entity);
        /** @var \Symfony\Component\Validator\ConstraintViolationList $errors*/
        $this->assertCount(0, $errors, (string) $errors);
        $em = self::$doctrine->getManagerForClass(SequenceableServiceTestEntity::class);
        $entity->setGroupId((string) $grouping);
        $em->persist($entity);
        $em->flush();
        $this->assertEquals(
            $expectedWithoutGrouping,
            $entity->getByService(),
            'byService after flush'
        );
        $this->assertEquals(
            $expectedWithGrouping,
            $entity->getByServiceAndGrouping(),
            'ByServiceAndGrouping after flush'
        );
    }
}
