<?php

namespace Mgo\DoctrineExtension\Sequenceable;

use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\Mapping\ClassMetadata;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Component\PropertyAccess as PA;

class SequenceableHelper
{
    /** @var ObjectManager */
    private $em;

    public static function getPropertyAccessor(): PA\PropertyAccessorInterface
    {
        return PA\PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->getPropertyAccessor();
    }

    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    public function getLastItemOfSequenceQueryBuilder(
        object $object,
        string $field,
        array $configuration,
        ?string $alias = null
    ): QueryBuilder {
        $alias = $alias ?? 't';
        // get PropertyAccessor
        $pa = self::getPropertyAccessor();
        /** @var \Doctrine\ORM\EntityRepository $repo */
        $repo = $this->getRepository($object);
        $qb = $repo->createQueryBuilder($alias);

        // sequence field should not be empty
        $qb->andWhere("{$alias}.{$field} IS NOT NULL");

        // add grouping conditions
        if ($configuration['grouping'] ?? false) {
            foreach ((array) $configuration['grouping'] as $grouping) {
                $qb->andWhere("{$alias}.{$grouping} = :{$grouping}");
                $qb->setParameter($grouping, $pa->getValue($object, $grouping));
            }
        }

        $qb->orderBy("{$alias}.{$field}", 'DESC');
        $qb->setMaxResults(1);

        return $qb;
    }

    public function getRepository(object $object): ObjectRepository
    {
        return $this->em->getRepository(\get_class($object));
    }

    public function getMetadata(object $object): ClassMetadata
    {
        return $this->em->getMetadataFactory()->getMetadataFor(\get_class($object));
    }
}
