<?php

namespace Test\Service;

use Mgo\DoctrineExtension\Sequenceable\Service\AbstractFormatterSequenceableService;

class FormatterSequenceableService extends AbstractFormatterSequenceableService
{
    private static $format = '\F\A-Y-m-d-\%\d';

    public static function setFormat(string $format): void
    {
        self::$format = $format;
    }

    protected function getFormat(): string
    {
        return self::$format;
    }
}
