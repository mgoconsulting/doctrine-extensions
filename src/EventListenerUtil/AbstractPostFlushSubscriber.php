<?php

namespace Mgo\DoctrineExtension\EventListenerUtil;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event;
use Doctrine\ORM\Events;
use Gedmo\SoftDeleteable\SoftDeleteableListener;

abstract class AbstractPostFlushSubscriber implements EventSubscriber
{
    private static $disabled = [];
    private static $map = [];

    abstract protected function getSubscribedClasses(): array;
    abstract protected function doPostFlush(object $entity, string $originEventName, EntityManagerInterface $em): void;

    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
            Events::postUpdate,
            Events::postRemove,
            Events::postFlush,
            SoftDeleteableListener::POST_SOFT_DELETE,
        ];
    }

    public function postPersist(Event\LifecycleEventArgs $eventArgs)
    {
        $this->listen($eventArgs, Events::postPersist);
    }

    public function postUpdate(Event\LifecycleEventArgs $eventArgs)
    {
        $this->listen($eventArgs, Events::postUpdate);
    }

    public function postRemove(Event\LifecycleEventArgs $eventArgs)
    {
        $this->listen($eventArgs, Events::postRemove);
    }

    public function postSoftDelete(Event\LifecycleEventArgs $eventArgs)
    {
        $this->listen($eventArgs, SoftDeleteableListener::POST_SOFT_DELETE);
    }

    public function postFlush(Event\PostFlushEventArgs $eventArgs)
    {
        if ($this->isEnabled()) {
            $subscriberClass = $this->getSubscriberClass();
            $this->disable(); // disable to avoid flush loops for same subscriber
            $map = self::$map[$subscriberClass] ?? [];
            unset(self::$map[$subscriberClass]);
            $em = $eventArgs->getEntityManager();
            foreach ($map as $originEventName => $events) {
                foreach ($events as $entity) {
                    $this->doPostFlush($entity, $originEventName, $em);
                }
            }
            $this->enable(); // re-enable subscriber
        }
    }

    private function listen(Event\LifecycleEventArgs $eventArgs, string $originEventName): void
    {
        $subscriberClass = $this->getSubscriberClass();
        if ($this->isEnabled()) {
            $entity = $eventArgs->getEntity();
            $possibilities = array_merge(
                [get_class($entity)],
                (\class_parents($entity) ?? []),   // @phpstan-ignore-line
                (\class_implements($entity) ?? []) // @phpstan-ignore-line
            );

            if (\array_intersect($this->getSubscribedClasses(), $possibilities)) {
                self::$map[$subscriberClass][$originEventName][] = $entity;
            }
        }
    }

    private function getSubscriberClass(): string
    {
        return get_class($this);
    }

    private function enable(): void
    {
        unset(self::$disabled[$this->getSubscriberClass()]);
    }

    private function disable(): void
    {
        self::$disabled[$this->getSubscriberClass()] = true;
    }

    private function isEnabled(): bool
    {
        return !isset(self::$disabled[$this->getSubscriberClass()]);
    }
}
