<?php

namespace Mgo\DoctrineExtension\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Callback as BaseCallback;

/**
 * @Annotation
 * @Target({"CLASS", "PROPERTY", "METHOD", "ANNOTATION"})
 */
class ServicedCallback extends BaseCallback
{
    /**
     * @var string|array
     */
    public $services;

    /**
     * @var string|array
     */
    public $parameters;

    /**
     * {@inheritdoc}
     */
    public function __construct($options = null)
    {
        // Invocation through annotations with an array parameter only
        if (\is_array($options) && 1 === \count($options) && isset($options['value'])) {
            $options = $options['value'];
        }

        if (
            \is_array($options)
            && !isset($options['callback'])
            && !isset($options['services'])
            && !isset($options['parameters'])
            && !isset($options['groups'])
            && !isset($options['payload'])
        ) {
            $options = ['callback' => $options];
        }

        Constraint::__construct($options);
    }
}
